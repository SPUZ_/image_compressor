# Image compressor

## Preview

That's working like that:
![This thing is working (lol)](photo.png "Working Thing")

It has .webp, .jpeg and slider to compress the image 

## Installing

1. git clone https://gitlab.com/SPUZ_/image_compressor
2. makefile

## Info

It's only for macOS or Linux (I haven't tested it on Windows yet, but I think it's not work on it because path on windows is complicated (/ - *nix, \ - windows))

## Created with 

1. Python 3.10
2. Pillow
3. PySide
4. pyinstaller
5. Emoji by Microsoft https://github.com/microsoft/fluentui-emoji
