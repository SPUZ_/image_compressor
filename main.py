import io
import os
import sys
from typing import Optional, Union, Any

from PySide6 import QtCore, QtWidgets, QtGui
from PIL.ImageQt import ImageQt
from PIL import Image
from PySide6.QtCore import Qt


def changeFormat(
    img: Any,
    img_format: str = ".webp",
    quality: int = 100,
    save_to_buffer: bool = True,
    path: str = "",
) -> Optional[Union[Any, int]]:
    """
    Changing format and quality of image | Can be used to save Image

    :param img: Image to change
    :param img_format: format to save [".webp", ".jpeg", ".png"]
    :param quality: Quality of the image to save
    :param save_to_buffer: True = used to save as preview to show in the application | False = save image, to path
    :param path: is the path for output file. Use only if save_to_buffer = False
    :returns: "[PIL.Image, int]" or "Nothing if save_to_buffer = False".
    """
    buffer = io.BytesIO()
    try:
        if save_to_buffer:
            img.save(buffer, format=img_format[1:], quality=quality)
        else:
            img.save(path, format=img_format[1:], quality=quality)
            return
    except Exception as e:
        if str(e) == "cannot write mode RGBA as JPEG":
            background = Image.new("RGB", img.size, (255, 255, 255))
            background.paste(img, mask=img.split()[3])  # 3 is the alpha channel
            if save_to_buffer:
                background.save(buffer, format=img_format[1:], quality=quality)
            else:
                background.save(path, format=img_format[1:], quality=quality)
                return

    buffer.seek(0)
    size = sys.getsizeof(buffer)
    img = Image.open(buffer)
    return img, size


def returnPix(img: Any) -> QtGui.QPixmap:
    """
    Pillow Image to pix for preview

    :param img: PIL.Image input
    :returns: QtGui.QPixmap as output
    """
    qim = ImageQt(img)
    pix = QtGui.QPixmap.fromImage(qim)
    pix = pix.scaledToHeight(700)
    pix = pix.scaledToWidth(700)
    return pix


class Selector(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()

        self.File = ""

        self.setWindowTitle("Image Compressor")

        layout = QtWidgets.QVBoxLayout(self)

        button = QtWidgets.QPushButton("Select Image (.jpg, .png, .jpeg, .webp)")
        button.clicked.connect(self.fileFinder)

        layout.addWidget(button)

        self.resize(500, 500)
        self.show()

    def fileFinder(self):
        file = QtWidgets.QFileDialog(
            self, "Select Image", "", "Image Files (*.jpg *.png *.jpeg *.webp)"
        )
        file.show()
        if file.exec():
            self.File = file.selectedFiles()[0]
            self.close()


class Viewer(QtWidgets.QWidget):
    def __init__(self, filename):
        super().__init__()

        self.File = filename
        self.setWindowTitle(f"Image Compressor {filename}")

        # Old Photo preview
        layout_old = QtWidgets.QVBoxLayout()

        size = os.path.getsize(self.File)
        im = Image.open(self.File)
        # Making old photo as "global" in this class
        self.original = im
        qim = ImageQt(im)
        pix = QtGui.QPixmap.fromImage(qim)
        # Height and Width of original photo to preview
        pix = pix.scaledToHeight(700)
        pix = pix.scaledToWidth(700)
        label = QtWidgets.QLabel()
        label.setPixmap(pix)

        text = QtWidgets.QLabel(f"Size: {size}", alignment=QtCore.Qt.AlignCenter)
        layout_old.addWidget(label)
        layout_old.addWidget(text)

        layout_new = QtWidgets.QVBoxLayout()

        im = Image.open(self.File)
        self.new_image = QtWidgets.QLabel()
        self.new_image.setPixmap(returnPix(im))

        self.slider = QtWidgets.QSlider(Qt.Horizontal)
        self.slider.setMinimum(1)
        self.slider.setMaximum(100)
        self.slider.setValue(100)
        self.slider.valueChanged.connect(self.valueChangedSlider)

        self.combo = QtWidgets.QComboBox()
        self.formats = [".webp", ".jpeg", ".png"]
        self.combo.addItems(self.formats)
        self.combo.currentIndexChanged.connect(self.valueChangedCombo)

        self.new_size_text = QtWidgets.QLabel(
            "Size: X", alignment=QtCore.Qt.AlignCenter
        )

        self.button = QtWidgets.QPushButton("Save!")
        self.button.clicked.connect(self.save)

        layout_new.addWidget(self.new_image)
        layout_new.addWidget(self.new_size_text)
        layout_new.addWidget(self.slider)
        layout_new.addWidget(self.combo)
        layout_new.addWidget(self.button)

        vertical_layout = QtWidgets.QHBoxLayout(self)
        vertical_layout.addLayout(layout_old)
        vertical_layout.addLayout(layout_new)

        self.setLayout(vertical_layout)

        self.show()

    def valueChangedSlider(self):
        img, size = changeFormat(
            self.original, self.combo.currentText(), self.slider.value()
        )
        self.new_image.setPixmap(returnPix(img))
        self.new_size_text.setText(f"Size ≈ {size}")

    def valueChangedCombo(self):
        img, size = changeFormat(
            self.original, self.combo.currentText(), self.slider.value()
        )
        self.new_image.setPixmap(returnPix(img))
        self.new_size_text.setText(f"Size ≈ {size}")

    def save(self):
        path = self.File
        path = path.split("/")
        filename = path[-1].split(".")[0] + "_compressed" + self.combo.currentText()
        path = path[:-1]
        path = "/".join(path) + "/" + filename
        changeFormat(
            self.original,
            self.combo.currentText(),
            self.slider.value(),
            save_to_buffer=False,
            path=path,
        )


class MainProgram(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()

        self.FilePath = ""

        self.wid = Selector()

        def closedSelector(event):
            event.accept()
            self.FilePath = self.wid.File

            if self.FilePath == "":
                self.close()
            else:
                self.viewer = Viewer(self.FilePath)

        self.wid.closeEvent = closedSelector


if __name__ == "__main__":
    app = QtWidgets.QApplication([])

    MainProgram()

    sys.exit(app.exec())
