all: build

build:
	pyinstaller -n "Image Compressor" --icon=icon_for_app.icns --windowed main.py
	mkdir release
	mv dist/Image\ Compressor.app release/Image\ Compressor.app
	rm -rf build/
	rm -rf Image\ Compressor.spec
	rm -rf dist/
